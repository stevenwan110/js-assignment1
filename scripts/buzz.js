/* File: buzz.js
 * Author: John Wan-You-Sew
 */
var countF=0;
var countB=0;
for (var i = 1; i<=100; i++){
    if(i%3 ==0){
        document.write('Fizz<br>');
        countF++;
    }else if(i%5 ==0){
        document.write('Buzz<br>');
        countB++;
    }else{
        document.write(i+'<br>');
    }
}
document.write('\"Fizz\" has been printed '+countF+' times<br>');
document.write('\"Buzz\" has been printed '+countB+' times<br>');

/* Description: Write a program that uses document.write to print all the numbers fro 1 too 100.
 * For numbers divisible by 3, print "Fizz" instead fo the number, and for numbers divisible by 5
 * (and not 3), print "Buzz" instead. Keep track of how many time Fizz is printed, and how many
 * times Buzz is printed.  Print the counts for both of these. (5 marks)
 */

