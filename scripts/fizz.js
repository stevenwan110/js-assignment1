/* File: fizz.js
 * Author: John Wan-You-Sew
 */

var countF=0;
for (var i = 1; i<=100; i++){
    if(i%3 ==0){
        document.write('Fizz<br>');
        countF++;
    }else{
        document.write(i+'<br>');
    }
}
document.write('\"Fizz\" has been printed '+countF+' times');
/* Description: Write a program that uses document.write to print all the numbers fro 1 too 100.
 * For numbers divisible by 3, print "Fizz" instead fo the number. Keep track of how many times
 * "Fizz" is printed and write that out to the document. (5 marks)
 */

