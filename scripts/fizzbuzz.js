/* File: fizzbuzz.js
 * Author: John Wan-You-Sew
 */
    var countF=0;
    var countB=0;
    var countFB=0;
    for (var i = 1; i<=100; i++){
        if(i%3 ==0 && i%5 ==0){
            document.write('FizzBuzz<br>');
            countFB++;
        }else if(i%3 ==0){
            document.write('Fizz<br>');
            countF++;
        }else if(i%5 ==0){
            document.write('Buzz<br>');
            countB++;
        }else{
            document.write(i+'<br>');
        }
    }
    document.write('\"Fizz\" has been printed '+countF+' times<br>');
    document.write('\"Buzz\" has been printed '+countB+' times<br>');
    document.write('\"FizzBuzz\" has been printed '+countFB+' times<br>');

/*
 * Description: Now modify the program to print "FizzBuzz" for numbers that are divisible by both 3
 * and 5 (and still print "Fizz" or "Buzz" for numbers divisible by only one of those).  Keep count
 * of how many times "Fizz", "Buzz" and "FizzBuzz" are written, and write those counts out to the
 * document. (5 marks)
 */
